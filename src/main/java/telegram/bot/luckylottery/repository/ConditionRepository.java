package telegram.bot.luckylottery.repository;

import telegram.bot.luckylottery.entity.Condition;
import telegram.bot.luckylottery.entity.Event;

import java.util.Set;

public interface ConditionRepository {
    void add(Condition condition);
    void edit(Condition condition);
    void delete(Condition condition);
    Set<Condition> findEventConditions(Event event);
}
