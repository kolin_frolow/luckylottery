package telegram.bot.luckylottery.repository;

import telegram.bot.luckylottery.entity.DBUser;
import telegram.bot.luckylottery.entity.Event;

import java.util.List;
import java.util.Set;

public interface EventRepository {
    void add(Event event);
    void edit(Event event);
    void delete(Event event);
    Event findLatestEvent();
    Set<Event> findAll();
    Boolean participate(Long event_id, Long user_id);
    List<DBUser> findEventUsers(Long event_id);
}
