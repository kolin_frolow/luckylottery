package telegram.bot.luckylottery.repository;

import telegram.bot.luckylottery.entity.Product;

import java.util.List;

public interface ProductRepository {
    void add(Product product);
    void addRelation(Long userId, Long productId);
    void delete(Product product);
    void edit(Product product);

    Product find(Long id);
    List<Product> findAll();
    List<Product> findShippedProducts(Long telegramId);
}
