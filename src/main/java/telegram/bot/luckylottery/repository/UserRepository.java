package telegram.bot.luckylottery.repository;

import telegram.bot.luckylottery.entity.DBUser;
import telegram.bot.luckylottery.entity.Event;

import java.util.Set;

public interface UserRepository {
    void add(DBUser DBUser);

    void addRelation(Long user_id, Long event_id);

    void delete(DBUser DBUser);

    void edit(DBUser DBUser);

    DBUser find(Long id);

    Set<DBUser> findEventUsers(Event event);

    Integer countEventUsers(Event event);
}
