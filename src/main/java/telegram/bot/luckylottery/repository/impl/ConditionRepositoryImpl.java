package telegram.bot.luckylottery.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Repository;
import telegram.bot.luckylottery.entity.Condition;
import telegram.bot.luckylottery.entity.Event;
import telegram.bot.luckylottery.repository.ConditionRepository;

import java.sql.Types;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Repository("conditionRepository")
public class ConditionRepositoryImpl implements ConditionRepository {
    @Autowired
    protected JdbcOperations jdbcOperations;

    @Override
    public void add(Condition condition) {
        String sql = "INSERT INTO public.condition (condition, event_id) VALUES (?, ?)";

        jdbcOperations.update(
                sql,
                new Object[]{ condition.getCondition(), condition.getEventId() },
                new int[]{ Types.BIGINT, Types.BIGINT });

    }

    @Override
    public void edit(Condition condition) {
        throw new UnsupportedOperationException("Operation is not supported yet!");
    }

    @Override
    public void delete(Condition condition) {
        throw new UnsupportedOperationException("Operation is not supported yet!");
    }

    @Override
    public Set<Condition> findEventConditions(Event event) {
        String sql = "SELECT id, condition, event_id " +
                "FROM public.condition " +
                "WHERE event_id=?";

        Set<Condition> conditions = new HashSet<>();

        List<Map<String, Object>> resultList = jdbcOperations.queryForList(
                sql,
                new Object[] { event.getId() },
                new int[] { Types.BIGINT }
        );

        for (Map<String, Object> row: resultList) {
            if (row != null) {
                Long id = (Long) row.get(Condition.ID);
                String conditionDescription = (String) row.get(Condition.CONDITION);
                Long eventId = (Long) row.get(Condition.EVENT_ID);

                conditions.add(new Condition(id, conditionDescription, eventId));
            }
        }

        return conditions;
    }
}
