package telegram.bot.luckylottery.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Repository;
import telegram.bot.luckylottery.entity.DBUser;
import telegram.bot.luckylottery.entity.Event;
import telegram.bot.luckylottery.repository.EventRepository;

import java.sql.Timestamp;
import java.sql.Types;
import java.util.*;

@Repository("eventRepository")
public class EventRepositoryImpl implements EventRepository {
    @Autowired
    protected JdbcOperations jdbcOperations;

    @Override
    public void add(Event event) {
        String sql = "INSERT INTO public.event (title, description, creation_date, active) VALUES(?, ?, ?, true)";

        jdbcOperations.update(
                sql,
                new Object[] { event.getTitle(), event.getDescription(), event.getCreationDate() },
                new int[] { Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP }
        );
    }

    @Override
    public void edit(Event event) {
        String sql = "UPDATE public.event SET title=?, description=? WHERE id=?";

        jdbcOperations.update(
                sql,
                new Object[] { event.getTitle(), event.getDescription(), event.getId() },
                new int[] { Types.VARCHAR, Types.VARCHAR, Types.BIGINT }
        );
    }

    @Override
    public void delete(Event event) {
        String sql = "UPDATE public.event SET active=false WHERE id=?";

        jdbcOperations.update(
                sql,
                new Object[] { event.getId() },
                new int[] { Types.BIGINT }
        );
    }

    @Override
    public Event findLatestEvent() {
        String sql = "SELECT id, title, description, creation_date, active " +
                "FROM public.event " +
                "WHERE active=true " +
                "ORDER BY creation_date DESC " +
                "LIMIT 1";

        List<Map<String, Object>> resultList = jdbcOperations.queryForList(
                sql
        );

        Event event = null;

        if (!resultList.isEmpty()) {
            Map<String, Object> row = resultList.get(0);

            Long id = (Long) row.get(Event.ID);
            String title = (String) row.get(Event.TITLE);
            String description = (String) row.get(Event.DESCRIPTION);
            Timestamp creationDate = (Timestamp) row.get(Event.CREATION_DATE);
            Boolean active = (Boolean) row.get(Event.ACTIVE);

            event = new Event(id, title, description, creationDate, active, null, null);
        }

        return event;
    }

    @Override
    public Set<Event> findAll() {
        String sql = "SELECT id, title, description, creation_date, active " +
                "FROM public.event " +
                "WHERE active=true " +
                "ORDER BY creation_date DESC";

        Set<Event> events = new HashSet<>();

        List<Map<String, Object>> resultList = jdbcOperations.queryForList(sql);

        for (Map<String, Object> row: resultList) {
            if (row != null) {
                Long id = (Long) row.get(Event.ID);
                String title = (String) row.get(Event.TITLE);
                String description = (String) row.get(Event.DESCRIPTION);
                Timestamp creationDate = (Timestamp) row.get(Event.CREATION_DATE);
                Boolean active = (Boolean) row.get(Event.ACTIVE);

                events.add(new Event(
                        id,
                        title,
                        description,
                        creationDate,
                        active,
                        null,
                        null
                ));
            }
        }

        return events;
    }

    @Override
    public Boolean participate(Long event_id, Long user_id) {
        String sql = "SELECT COUNT(*) " +
                "FROM public.event_user eu " +
                "JOIN public.user u " +
                "ON eu.user_id = u.telegram_id " +
                "WHERE u.telegram_id=? AND eu.event_id=?";

        Integer count = jdbcOperations.queryForObject(
                sql,
                new Object[] { user_id, event_id },
                Integer.class
        );

        return count > 0;
    }

    @Override
    public List<DBUser> findEventUsers(Long event_id) {
        String sql = "SELECT u.telegram_id, u.username, u.role, u.first_name, u.last_name, u.ref_id, u.cash " +
                "FROM public.event_user eu " +
                "JOIN public.user u " +
                "ON u.telegram_id = eu.user_id " +
                "WHERE eu.event_id=?";

        List<Map<String, Object>> queryList =
                jdbcOperations.queryForList(sql, new Object[] { event_id }, new int[] { Types.BIGINT });

        List<DBUser> eventUsers = new ArrayList<>();

        for (Map<String, Object> row: queryList) {
            Long telegram_id = (Long) row.get("telegram_id");
            String username = (String) row.get("username");
            Integer role = (Integer) row.get("role");
            String firstName = (String) row.get("first_name");
            String lastName = (String) row.get("last_name");
            Long refId = (Long) row.get(DBUser.REF_ID);
            Double cash = (Double) row.get(DBUser.CASH);

            eventUsers.add(new DBUser(telegram_id, username, firstName, lastName, 0, role, refId, cash));
        }

        return eventUsers;
    }
}
