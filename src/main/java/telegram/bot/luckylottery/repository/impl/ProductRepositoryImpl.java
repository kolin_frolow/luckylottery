package telegram.bot.luckylottery.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Repository;
import telegram.bot.luckylottery.entity.Product;
import telegram.bot.luckylottery.repository.ProductRepository;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository("productRepository")
public class ProductRepositoryImpl implements ProductRepository {
    @Autowired
    protected JdbcOperations jdbcOperations;

    private static final String SQL_ADD_RELATION = "INSERT INTO public.product_user (telegram_id, product_id, creation_date) VALUES (?, ?, NOW())";
    private static final String SQL_DELETE = "UPDATE public.product SET active=false WHERE telegram_id=?";
    private static final String SQL_EDIT = "UPDATE public.product SET price=?, title=?, actual=? WHERE id=?";
    private static final String SQL_FIND_ID = "SELECT id, price, title, actual FROM public.product WHERE id=?";
    private static final String SQL_FIND_ALL = "SELECT id, price, title, actual FROM public.product " +
            "WHERE actual=true";
    private static final String SQL_FIND_SHIPPED_PRODUCTS = "SELECT pu.telegram_id, pu.product_id, p.price, p.title, p.actual " +
            "FROM public.product_user pu " +
            "JOIN public.product p " +
            "ON pu.product_id = p.id " +
            "WHERE pu.telegram_id=? AND pu.active=true " +
            "ORDER BY pu.creation_date";


    @Override
    public void add(Product product) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addRelation(Long userId, Long productId) {
        jdbcOperations.update(
            SQL_ADD_RELATION,
            new Object[]{userId, productId},
            new int[]{ Types.BIGINT, Types.BIGINT });
    }

    @Override
    public void delete(Product product) {
        jdbcOperations.update(
            SQL_DELETE,
            new Object[] { product.getId() },
            new int[] { Types.BIGINT }
        );
    }

    @Override
    public void edit(Product product) {
        jdbcOperations.update(
            SQL_EDIT,
            new Object[] { product.getPrice(), product.getTitle(), product.getActual(), product.getId() },
            new int[] { Types.DOUBLE, Types.VARCHAR, Types.BOOLEAN, Types.BIGINT }
        );
    }

    @Override
    public Product find(Long id) {
        Product product = null;

        List<Map<String, Object>> resultList = jdbcOperations.queryForList(
                SQL_FIND_ID,
                new Object[] { id },
                new int[] { Types.BIGINT }
        );

        if (!resultList.isEmpty()) {
            Map<String, Object> row = resultList.get(0);

            if (row != null) {
                String title = (String) row.get(Product.TITLE);
                Double price = (Double) row.get(Product.PRICE);
                Boolean actual = (Boolean) row.get(Product.ACTUAL);

                product = new Product(id, price, title, actual);
            }
        }

        return product;
    }

    @Override
    public List<Product> findAll() {
        List<Product> productList = new ArrayList<>();

        List<Map<String, Object>> resultList = jdbcOperations.queryForList(
                SQL_FIND_ALL
        );


        for (Map<String, Object> row: resultList) {
            if (row != null) {
                Long id = (Long) row.get(Product.ID);
                String title = (String) row.get(Product.TITLE);
                Double price = (Double) row.get(Product.PRICE);
                Boolean actual = (Boolean) row.get(Product.ACTUAL);

                productList.add(new Product(id, price, title, actual));
            }
        }

        return productList;
    }

    @Override
    public List<Product> findShippedProducts(Long telegramId) {
        List<Product> productList = new ArrayList<>();

        List<Map<String, Object>> resultList = jdbcOperations.queryForList(
                SQL_FIND_SHIPPED_PRODUCTS,
                new Object[] { telegramId },
                new int[] { Types.BIGINT }
        );


        for (Map<String, Object> row: resultList) {
            if (row != null) {
                Long id = (Long) row.get(Product.ID);
                String title = (String) row.get(Product.TITLE);
                Double price = (Double) row.get(Product.PRICE);
                Boolean actual = (Boolean) row.get(Product.ACTUAL);

                productList.add(new Product(id, price, title, actual));
            }
        }

        return productList;
    }
}
