package telegram.bot.luckylottery.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Repository;
import telegram.bot.luckylottery.entity.DBUser;
import telegram.bot.luckylottery.entity.Event;
import telegram.bot.luckylottery.repository.UserRepository;

import java.sql.Types;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Repository("userRepository")
public class UserRepositoryImpl implements UserRepository {
    @Autowired
    protected JdbcOperations jdbcOperations;

    @Override
    public void add(DBUser dbUser) {
        String sql = "INSERT INTO public.user (telegram_id, username, ref_count, role, first_name, last_name, ref_id) VALUES (?, ?, 0, 0, ?, ?, ?)";

        jdbcOperations.update(
                sql,
                new Object[]{ dbUser.getTelegramId(), dbUser.getUserName(), dbUser.getFirstName(), dbUser.getLastName(), dbUser.getRefId() },
                new int[]{ Types.BIGINT, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.BIGINT });
    }

    @Override
    public void addRelation(Long user_id, Long event_id) {
        String sql = "INSERT INTO public.event_user(user_id, event_id) VALUES (?, ?)";

        jdbcOperations.update(
                sql,
                new Object[]{ user_id, event_id },
                new int[]{ Types.BIGINT, Types.BIGINT });
    }

    @Override
    public void delete(DBUser dbUser) {
        throw new UnsupportedOperationException("Not implemented yet!");
    }

    @Override
    public void edit(DBUser dbUser) {
        String sql = "UPDATE public.user SET ref_count=?, role=?, cash=? WHERE telegram_id=?";

        jdbcOperations.update(
                sql,
                new Object[] { dbUser.getRefCount(), dbUser.getRole(), dbUser.getCash(), dbUser.getTelegramId() },
                new int[] { Types.INTEGER, Types.INTEGER, Types.DOUBLE, Types.BIGINT }
        );
    }

    @Override
    public DBUser find(Long id) {
        String sql = "SELECT telegram_id, username, ref_count, first_name, last_name, role, cash, ref_id " +
                "FROM public.user WHERE telegram_id=?";

        List<Map<String, Object>> resultList = jdbcOperations.queryForList(
                sql,
                new Object[] { id },
                new int[] { Types.BIGINT }
        );

        DBUser user = null;

        if (!resultList.isEmpty()) {
            Map<String, Object> row = resultList.get(0);

            Long telegramId = (Long) row.get(DBUser.TELEGRAM_ID);
            String userName = (String) row.get(DBUser.USER_NAME);
            String firstName = (String) row.get(DBUser.FIRST_NAME);
            String lastName = (String) row.get(DBUser.LAST_NAME);
            Integer refCount = (Integer) row.get(DBUser.REF_COUNT);
            Integer role = (Integer) row.get(DBUser.ROLE);
            Long refId = (Long) row.get(DBUser.REF_ID);
            Double cash = (Double) row.get(DBUser.CASH);

            user = new DBUser(telegramId, userName, firstName, lastName, refCount, role, refId, cash);
        }

        return user;
    }

    @Override
    public Set<DBUser> findEventUsers(Event event) {
        String sql = "SELECT event_user.telegram_id, user.username, user.ref_count, user.first_name, user.last_name, user.role " +
                "FROM public.event_user " +
                "JOIN public.user " +
                "ON event_user.user_id = user.id " +
                "WHERE event_id=? AND user.role=0";

        Set<DBUser> DBUsers = new HashSet<>();

        List<Map<String, Object>> resultList = jdbcOperations.queryForList(
                sql,
                event.getId(),
                new int[] { Types.BIGINT }
            );


        for (Map<String, Object> row: resultList) {
            if (row != null) {
                Long telegramId = (Long) row.get(DBUser.TELEGRAM_ID);
                String userName = (String) row.get(DBUser.USER_NAME);
                String firstName = (String) row.get(DBUser.FIRST_NAME);
                String lastName = (String) row.get(DBUser.LAST_NAME);
                Integer refCount = (Integer) row.get(DBUser.REF_COUNT);
                Integer role = (Integer) row.get(DBUser.ROLE);
                Long refId = (Long) row.get(DBUser.REF_ID);
                Double cash = (Double) row.get(DBUser.CASH);

                DBUsers.add(new DBUser(telegramId, userName, firstName, lastName, refCount, role, refId, cash));
            }
        }

        return DBUsers;
    }

    @Override
    public Integer countEventUsers(Event event) {
        String sql = "SELECT COUNT(*) " +
                "FROM public.event_user eu " +
                "JOIN public.user u " +
                "ON eu.user_id = u.telegram_id " +
                "WHERE event_id=?";

        Integer count = jdbcOperations.queryForObject(
                sql,
                new Object[] { event.getId() },
                Integer.class
            );

        return count;
    }
}
