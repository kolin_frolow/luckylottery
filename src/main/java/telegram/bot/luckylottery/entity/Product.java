package telegram.bot.luckylottery.entity;

import java.util.Objects;

public class Product implements DomainObject {
    public static final String ID = "ID";
    public static final String PRICE = "price";
    public static final String TITLE = "title";
    public static final String ACTUAL = "actual";

    private Long id;
    private Double price;
    private String title;
    private Boolean actual;

    public Product(Long id, Double price, String title, Boolean actual) {
        this.id = id;
        this.price = price;
        this.title = title;
        this.actual = actual;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getActual() {
        return actual;
    }

    public void setActual(Boolean actual) {
        this.actual = actual;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id) &&
                Objects.equals(price, product.price) &&
                Objects.equals(title, product.title) &&
                Objects.equals(actual, product.actual);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, price, title, actual);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", price=" + price +
                ", title='" + title + '\'' +
                ", actual=" + actual +
                '}';
    }
}
