package telegram.bot.luckylottery.entity;

import java.util.Objects;

public class Condition implements DomainObject {
    public static final String ID = "id";
    public static final String CONDITION = "condition";
    public static final String EVENT_ID = "event_id";

    private Long id;
    private String condition;
    private Long eventId;

    public Condition(Long id, String condition, Long eventId) {
        this.id = id;
        this.condition = condition;
        this.eventId = eventId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Condition condition1 = (Condition) o;
        return Objects.equals(id, condition1.id) &&
                Objects.equals(condition, condition1.condition) &&
                Objects.equals(eventId, condition1.eventId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, condition, eventId);
    }
}
