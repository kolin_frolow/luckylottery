package telegram.bot.luckylottery.entity;

import java.util.Objects;

public class DBUser implements DomainObject {
    public static final String TELEGRAM_ID = "telegram_id";
    public static final String USER_NAME = "user_name";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String REF_COUNT = "ref_count";
    public static final String ROLE = "role";
    public static final String CASH = "cash";
    public static final String REF_ID = "ref_id";

    private Long telegramId;
    private String userName;
    private String firstName;
    private String lastName;
    private Integer refCount;
    private Integer role;
    private Long refId;
    private Double cash;

    public DBUser(Long telegramId, String userName, String firstName, String lastName, Integer refCount, Integer role, Long refId, Double cash) {
        this.telegramId = telegramId;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.refCount = refCount;
        this.role = role;
        this.refId = refId;
        this.cash = cash;
    }

    public Long getTelegramId() {
        return telegramId;
    }

    public void setTelegramId(Long telegramId) {
        this.telegramId = telegramId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getRefCount() {
        return refCount;
    }

    public void setRefCount(Integer refCount) {
        this.refCount = refCount;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public Long getRefId() {
        return refId;
    }

    public void setRefId(Long refId) {
        this.refId = refId;
    }

    public Double getCash() {
        return cash;
    }

    public void setCash(Double cash) {
        this.cash = cash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DBUser dbUser = (DBUser) o;
        return Objects.equals(telegramId, dbUser.telegramId) &&
                Objects.equals(userName, dbUser.userName) &&
                Objects.equals(firstName, dbUser.firstName) &&
                Objects.equals(lastName, dbUser.lastName) &&
                Objects.equals(refCount, dbUser.refCount) &&
                Objects.equals(role, dbUser.role) &&
                Objects.equals(refId, dbUser.refId) &&
                Objects.equals(cash, dbUser.cash);
    }

    @Override
    public int hashCode() {

        return Objects.hash(telegramId, userName, firstName, lastName, refCount, role, refId, cash);
    }

    @Override
    public String toString() {
        return "DBUser{" +
                "telegramId=" + telegramId +
                ", userName='" + userName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", refCount=" + refCount +
                ", role=" + role +
                ", refId=" + refId +
                ", cash=" + cash +
                '}';
    }
}
