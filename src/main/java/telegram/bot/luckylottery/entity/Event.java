package telegram.bot.luckylottery.entity;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.Set;

public class Event implements DomainObject {
    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String CREATION_DATE = "creation_date";
    public static final String ACTIVE = "active";

    private Long id;
    private String title;
    private String description;
    private Timestamp creationDate;
    private Boolean active;
    private Set<Condition> conditions;
    private Set<DBUser> DBUsers;

    public Event(Long id, String title, String description, Timestamp creationDate, Boolean active, Set<Condition> conditions, Set<DBUser> DBUsers) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.creationDate = creationDate;
        this.active = active;
        this.conditions = conditions;
        this.DBUsers = DBUsers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(Set<Condition> conditions) {
        this.conditions = conditions;
    }

    public Set<DBUser> getDBUsers() {
        return DBUsers;
    }

    public void setDBUsers(Set<DBUser> DBUsers) {
        this.DBUsers = DBUsers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return Objects.equals(id, event.id) &&
                Objects.equals(title, event.title) &&
                Objects.equals(description, event.description) &&
                Objects.equals(creationDate, event.creationDate) &&
                Objects.equals(active, event.active) &&
                Objects.equals(conditions, event.conditions) &&
                Objects.equals(DBUsers, event.DBUsers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, creationDate, active, conditions, DBUsers);
    }
}
