package telegram.bot.luckylottery.entity;

import java.util.Objects;

public class User implements DomainObject {
    public static final String TELEGRAM_ID = "telegram_id";
    public static final String REF_LINK = "ref_link";
    public static final String USER_NAME = "user_name";
    public static final String REF_COUNT = "ref_count";
    public static final String ROLE = "role";

    private Long telegramId;
    private String refLink;
    private String userName;
    private Integer refCount;
    private Integer role;

    public User(Long telegramId, String refLink, String userName, Integer refCount, Integer role) {
        this.telegramId = telegramId;
        this.refLink = refLink;
        this.userName = userName;
        this.refCount = refCount;
        this.role = role;
    }

    public Long getTelegramId() {
        return telegramId;
    }

    public void setTelegramId(Long telegramId) {
        this.telegramId = telegramId;
    }

    public String getRefLink() {
        return refLink;
    }

    public void setRefLink(String refLink) {
        this.refLink = refLink;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getRefCount() {
        return refCount;
    }

    public void setRefCount(Integer refCount) {
        this.refCount = refCount;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(telegramId, user.telegramId) &&
                Objects.equals(refLink, user.refLink) &&
                Objects.equals(userName, user.userName) &&
                Objects.equals(refCount, user.refCount) &&
                Objects.equals(role, user.role);
    }

    @Override
    public int hashCode() {

        return Objects.hash(telegramId, refLink, userName, refCount, role);
    }
}
