package telegram.bot.luckylottery.entity;

public enum Role {
    MEMBER(0),
    BANNED(1),
    MODER(2),
    ADMIN(3);

    Integer roleId;
    Role(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}
