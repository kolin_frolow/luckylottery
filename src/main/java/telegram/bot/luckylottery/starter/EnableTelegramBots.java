package telegram.bot.luckylottery.starter;

import org.springframework.context.annotation.Import;

@Import(TelegramBotStarterConfiguration.class)
public @interface EnableTelegramBots {
}
