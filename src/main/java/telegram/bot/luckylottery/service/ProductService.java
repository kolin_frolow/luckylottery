package telegram.bot.luckylottery.service;

import telegram.bot.luckylottery.entity.Product;

import java.util.List;

public interface ProductService {
    void add(Product product);
    Boolean buy(Long userId, Long productId);
    void delete(Product product);
    void edit(Product product);

    Product find(Long id);
    List<Product> findAll();
    List<Product> findShippedProducts(Long telgramId);
}
