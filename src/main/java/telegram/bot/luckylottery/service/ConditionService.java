package telegram.bot.luckylottery.service;

import telegram.bot.luckylottery.entity.Condition;
import telegram.bot.luckylottery.entity.Event;

import java.util.Set;

public interface ConditionService {
    void add(Condition condition);
    void edit(Condition condition);
    void delete(Condition condition);
}
