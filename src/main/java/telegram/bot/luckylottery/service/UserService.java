package telegram.bot.luckylottery.service;

import telegram.bot.luckylottery.entity.DBUser;

public interface UserService {
    void add(DBUser DBUser);

    void delete(DBUser DBUser);

    void edit(DBUser DBUser);

    DBUser find(Long id);

    void participate(Long user_id, Long event_id);
}
