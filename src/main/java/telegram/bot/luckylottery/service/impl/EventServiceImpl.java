package telegram.bot.luckylottery.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telegram.bot.luckylottery.entity.Condition;
import telegram.bot.luckylottery.entity.Event;
import telegram.bot.luckylottery.entity.DBUser;
import telegram.bot.luckylottery.repository.ConditionRepository;
import telegram.bot.luckylottery.repository.EventRepository;
import telegram.bot.luckylottery.repository.UserRepository;
import telegram.bot.luckylottery.service.EventService;

import java.util.List;
import java.util.Random;
import java.util.Set;

@Service("eventService")
public class EventServiceImpl implements EventService {
    @Autowired
    protected EventRepository eventRepository;

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected ConditionRepository conditionRepository;

    @Override
    public void add(Event event) {
        eventRepository.add(event);
    }

    @Override
    public void edit(Event event) {
        eventRepository.edit(event);
    }

    @Override
    public void delete(Event event) {
        eventRepository.delete(event);
    }

    @Override
    public Event findLatest() {
        Event event = eventRepository.findLatestEvent();

        if (event != null) {
            //Set<Condition> conditions = conditionRepository.findEventConditions(event);
            //event.setConditions(conditions);
        }

        return event;
    }

    @Override
    public Set<Event> findAll() {
        return eventRepository.findAll();
    }

    @Override
    public Integer countEventUsers(Event event) {
        return userRepository.countEventUsers(event);
    }

    @Override
    public List<DBUser> findEventUsers(Long event_id) {
        return eventRepository.findEventUsers(event_id);
    }

    @Override
    public DBUser findWinner(Event event) {
        Set<DBUser> DBUsers = userRepository.findEventUsers(event);

        int size = DBUsers.size();
        int item = new Random().nextInt(size);
        int i = 0;
        for(Object obj : DBUsers)
        {
            if (i == item)
                return (DBUser) obj;
            i++;
        }

        return null;
    }

    @Override
    public Boolean participate(Long event_id, Long user_id) {
        return eventRepository.participate(event_id, user_id);
    }
}
