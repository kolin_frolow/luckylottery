package telegram.bot.luckylottery.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telegram.bot.luckylottery.entity.Condition;
import telegram.bot.luckylottery.repository.ConditionRepository;
import telegram.bot.luckylottery.service.ConditionService;

@Service("conditionService")
public class ConditionServiceImpl implements ConditionService {
    @Autowired
    protected ConditionRepository conditionRepository;

    @Override
    public void add(Condition condition) {
        conditionRepository.add(condition);
    }

    @Override
    public void edit(Condition condition) {
        conditionRepository.add(condition);
    }

    @Override
    public void delete(Condition condition) {
        conditionRepository.delete(condition);
    }
}
