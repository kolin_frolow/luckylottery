package telegram.bot.luckylottery.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import telegram.bot.luckylottery.entity.DBUser;
import telegram.bot.luckylottery.repository.UserRepository;
import telegram.bot.luckylottery.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    protected UserRepository userRepository;

    @Override
    public void add(DBUser DBUser) {
        userRepository.add(DBUser);
    }

    @Override
    public void delete(DBUser DBUser) {
        userRepository.delete(DBUser);
    }

    @Override
    public void edit(DBUser DBUser) {
        userRepository.edit(DBUser);
    }

    @Override
    public DBUser find(Long id) {
        return userRepository.find(id);
    }

    @Override
    public void participate(Long user_id, Long event_id) {
        userRepository.addRelation(user_id, event_id);
    }
}
