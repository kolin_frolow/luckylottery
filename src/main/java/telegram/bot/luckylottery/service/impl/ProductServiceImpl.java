package telegram.bot.luckylottery.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import telegram.bot.luckylottery.entity.DBUser;
import telegram.bot.luckylottery.entity.Product;
import telegram.bot.luckylottery.repository.ProductRepository;
import telegram.bot.luckylottery.repository.UserRepository;
import telegram.bot.luckylottery.service.ProductService;

import java.util.List;

@Service("productService")
public class ProductServiceImpl implements ProductService {
    @Autowired
    protected ProductRepository productRepository;

    @Autowired
    protected UserRepository userRepository;

    @Override
    public void add(Product product) {
        productRepository.add(product);
    }

    @Override
    @Transactional
    public Boolean buy(Long userId, Long productId) {
        Boolean result = false;

        DBUser user = userRepository.find(userId);
        if (user != null) {
            //TODO: find by id
            Product product = productRepository.find(productId);
            if (product != null) {
                double price = product.getPrice();
                double cash = user.getCash();
                if (user.getCash() >= price) {
                    user.setCash(cash - price);
                    userRepository.edit(user);
                    productRepository.addRelation(userId, productId);

                    result = true;
                }
            }
        }

        return result;
    }

    @Override
    public void delete(Product product) {
        productRepository.delete(product);
    }

    @Override
    public void edit(Product product) {
        productRepository.edit(product);
    }

    @Override
    public Product find(Long id) {
        return productRepository.find(id);
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public List<Product> findShippedProducts(Long telgramId) {
        return productRepository.findShippedProducts(telgramId);
    }
}
