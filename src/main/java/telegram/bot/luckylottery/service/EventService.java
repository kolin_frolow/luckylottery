package telegram.bot.luckylottery.service;

import telegram.bot.luckylottery.entity.DBUser;
import telegram.bot.luckylottery.entity.Event;

import java.util.List;
import java.util.Set;

public interface EventService {
    void add(Event event);

    void edit(Event event);

    void delete(Event event);

    Event findLatest();

    Set<Event> findAll();

    Integer countEventUsers(Event event);

    DBUser findWinner(Event event);

    Boolean participate(Long event_id, Long user_id);

    List<DBUser> findEventUsers(Long event_id);
}
