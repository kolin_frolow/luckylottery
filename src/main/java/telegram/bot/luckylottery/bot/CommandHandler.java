package telegram.bot.luckylottery.bot;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

public interface CommandHandler {
    SendMessage handle(Update update, TelegramLongPollingBot bot) throws TelegramApiException;
}
