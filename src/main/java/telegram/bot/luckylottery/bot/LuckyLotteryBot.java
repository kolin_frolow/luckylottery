package telegram.bot.luckylottery.bot;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendDocument;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Component
public class LuckyLotteryBot extends TelegramLongPollingBot {
    @Autowired
    protected CommandHandler commandHandler;

    @Autowired
    protected AdminCommands adminCommands;

    private final static Logger logger = Logger.getLogger(LuckyLotteryBot.class);

    @Override
    public void onUpdateReceived(Update update) {

        try {
            /*
            String message = update.getMessage().getText();
            if (message != null && message.startsWith(Messages.A_GLOBAL)) {
                if (message.startsWith(Messages.A_USER_LIST)) {
                    SendDocument document = adminCommands.getEventUsersFile(update);
                    if (document != null) {
                        sendDocument(document);
                    }
                }
            } else {*/
                SendMessage sendMessage = commandHandler.handle(update, this);
                execute(sendMessage);
            //}
        } catch (TelegramApiException e) {
            logger.error(e);
        }
    }

    @Override
    public String getBotUsername() {
        return "luckylottery_bot";
    }

    @Override
    public String getBotToken() {
        return "559814114:AAFg_pQJZsW5ZZd7f9EFNBtQaPoLScmu00k";
    }
}
