package telegram.bot.luckylottery.bot.impl;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import telegram.bot.luckylottery.bot.BoardManager;
import telegram.bot.luckylottery.bot.Emoji;
import telegram.bot.luckylottery.bot.Messages;
import telegram.bot.luckylottery.entity.Condition;
import telegram.bot.luckylottery.entity.Event;
import telegram.bot.luckylottery.entity.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component("boardManager")
public class BoardManagerImpl implements BoardManager {
    @Override
    public SendMessage applyDefaultKeyboard(SendMessage sendMessage) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardRow keyboardFirstRow = new KeyboardRow();
        keyboardFirstRow.add(new KeyboardButton(Messages.M_EVENT));

        KeyboardRow keyboardSecondRow = new KeyboardRow();
        keyboardSecondRow.add(new KeyboardButton(Messages.M_REF_LINK));
        keyboardSecondRow.add(new KeyboardButton(Messages.M_NEWS));
        keyboardSecondRow.add(new KeyboardButton(Messages.M_SHOP));

        KeyboardRow keyboardThirdRow = new KeyboardRow();
        keyboardThirdRow.add(new KeyboardButton(Messages.M_HELP));

        keyboard.add(keyboardFirstRow);
        keyboard.add(keyboardSecondRow);
        keyboard.add(keyboardThirdRow);

        replyKeyboardMarkup.setKeyboard(keyboard);

        return sendMessage;
    }

    @Override
    public SendMessage applyEventKeyboard(SendMessage sendMessage) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardRow row = new KeyboardRow();
        row.add(new KeyboardButton(Messages.M_BACK));
        row.add(new KeyboardButton(Messages.M_PARTICIPATE));

        keyboard.add(row);

        replyKeyboardMarkup.setKeyboard(keyboard);

        return sendMessage;
    }

    @Override
    public SendMessage applyShopInlineKeyboard(SendMessage sendMessage, List<Product> productList) {
        List<List<InlineKeyboardButton>> buttons = new ArrayList<>();

        for (Product product:
             productList) {
            List<InlineKeyboardButton> rowButton = new ArrayList<>();
            rowButton.add(new InlineKeyboardButton()
                .setText(product.getTitle() + " - " + product.getPrice() + " points")
                .setCallbackData(Messages.I_BUY + product.getId()));

            buttons.add(rowButton);
        }

        InlineKeyboardMarkup markupKeyboard = new InlineKeyboardMarkup();
        markupKeyboard.setKeyboard(buttons);

        return sendMessage.setReplyMarkup(markupKeyboard);
    }

    @Override
    public SendMessage applyShopKeyboard(SendMessage sendMessage) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardRow row = new KeyboardRow();
        row.add(new KeyboardButton(Messages.M_BACK));
        row.add(new KeyboardButton(Messages.M_SHOP_LIST));

        keyboard.add(row);

        replyKeyboardMarkup.setKeyboard(keyboard);

        return sendMessage;
    }
}
