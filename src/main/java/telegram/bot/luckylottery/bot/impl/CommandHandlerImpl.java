package telegram.bot.luckylottery.bot.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import telegram.bot.luckylottery.bot.BoardManager;
import telegram.bot.luckylottery.bot.CommandHandler;
import telegram.bot.luckylottery.bot.Messages;
import telegram.bot.luckylottery.bot.ParseMode;
import telegram.bot.luckylottery.entity.DBUser;
import telegram.bot.luckylottery.entity.Event;
import telegram.bot.luckylottery.entity.Product;
import telegram.bot.luckylottery.entity.Role;
import telegram.bot.luckylottery.service.ConditionService;
import telegram.bot.luckylottery.service.EventService;
import telegram.bot.luckylottery.service.ProductService;
import telegram.bot.luckylottery.service.UserService;
import telegram.bot.luckylottery.util.RegexUtils;

import java.util.List;

@Component("commandHandler")
public class CommandHandlerImpl implements CommandHandler {
    @Autowired
    protected EventService eventService;

    @Autowired
    protected UserService userService;

    @Autowired
    protected ProductService productService;

    @Autowired
    protected ConditionService conditionService;

    @Autowired
    protected BoardManager boardManager;


    private static final Integer DEFAULT_REF_COUNT = 0;
    private static final Double DEFAULT_CASH_COUNT = 0.0;
    private static final Double DEFAULT_REF_CASH_INCREMENT = 100.0;

    @Override
    public SendMessage handle(Update update, TelegramLongPollingBot bot) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();

        if (update.hasMessage()) {
            String message = update.getMessage().getText();

            if (message != null) {
                if (message.startsWith(Messages.M_START)) {
                    sendMessage = startAcrtion(userService, boardManager, update);
                } else if (message.startsWith(Messages.M_REF_LINK)) {
                    sendMessage = refLinkAction(userService, boardManager, update);
                } else if (message.startsWith(Messages.M_EVENT)) {
                    sendMessage = eventAction(eventService, boardManager, update);
                } else if (message.startsWith(Messages.M_HELP)) {
                    sendMessage = helpAction(boardManager, update);
                } else if (message.startsWith(Messages.M_NEWS)) {
                    sendMessage = newsAction(boardManager, update);
                } else if (message.startsWith(Messages.M_SHOP)) {
                    sendMessage = shopAction(bot, boardManager, update);
                } else if (message.startsWith(Messages.M_SHOP_LIST)) {
                    sendMessage = shopListAction(boardManager, update);
                } else if (message.startsWith(Messages.M_BACK)) {
                    sendMessage = wrongAction(boardManager, update, Messages.R_WRONG_COMMAND);
                } else if (message.startsWith(Messages.M_PARTICIPATE)) {
                    sendMessage = participateAction(eventService, userService, boardManager, update);
                } else {
                    sendMessage = wrongAction(boardManager, update, Messages.R_WRONG_COMMAND);
                }
            }
        } else if (update.hasCallbackQuery()) {
            String command = update.getCallbackQuery().getData();
            if (command != null) {
                if (command.startsWith(Messages.I_BUY)) {
                    sendMessage = buyAction(command, boardManager, update, productService);
                }
            } else {
                sendMessage = wrongAction(boardManager, update, Messages.R_WRONG_COMMAND);
            }
            System.out.println("Callback query!");
        } else {
            sendMessage = wrongAction(boardManager, update, Messages.R_WRONG_COMMAND);
        }


        return sendMessage;
    }

    private SendMessage participateAction(EventService eventService, UserService userService,
                                          BoardManager boardManager, Update update) {
        SendMessage sendMessage = new SendMessage();
        Long id = update.getMessage().getChatId();
        Event event = eventService.findLatest();
        DBUser dbUser = userService.find(id);

        if (dbUser == null) {
            sendMessage = startAcrtion(userService, boardManager, update);
            dbUser = userService.find(id);
        }

        if (event != null && dbUser != null) {
            //TODO: check participation
            if( true ) {
                if (!eventService.participate(event.getId(), dbUser.getTelegramId())) {
                    userService.participate(dbUser.getTelegramId(), event.getId());
                    sendMessage.setText(Messages.R_PARTICIPATION_SUCCESSFULL);
                } else {

                    sendMessage.setText(Messages.R_PARTICIPATION_ALREADY);
                }

                boardManager.applyDefaultKeyboard(sendMessage);
            } else {
                sendMessage.setText(Messages.R_PARTICIPATION_FAIL);
                boardManager.applyEventKeyboard(sendMessage);
            }
        } else {
            sendMessage.setText(Messages.R_NO_EVENT)
                    .setParseMode(ParseMode.HTML);
            boardManager.applyDefaultKeyboard(sendMessage);
        }

        sendMessage.setChatId(id);

        return sendMessage;
    }

    private SendMessage shopListAction(BoardManager boardManager, Update update) {
        SendMessage sendMessage = new SendMessage();
        Long id = update.getMessage().getChatId();

        List<Product> productList = productService.findShippedProducts(id);
        sendMessage.setChatId(id);

        if (productList != null && !productList.isEmpty()) {
            StringBuilder text = new StringBuilder(Messages.R_SHOP_LIST);

            for (Product product:
                 productList) {
                text.append(product.getTitle());
                text.append("\n");
            }

            sendMessage.setText(text.toString());
            boardManager.applyDefaultKeyboard(sendMessage);
        } else {
            sendMessage.setText(Messages.R_SHOP_LIST_EMPTY);
            boardManager.applyDefaultKeyboard(sendMessage);
        }

        return sendMessage;
    }

    private SendMessage buyAction(String command, BoardManager boardManager, Update update,
                                  ProductService productService) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        Long id = update.getCallbackQuery().getFrom().getId().longValue();
        sendMessage.setChatId(id);

        Long productId = RegexUtils.extractArgument(command);

        if (productId != 0) {
            if (productService.buy(id, productId)) {
                sendMessage.setText(Messages.R_BUY_SUCCESS);
            } else {
                sendMessage.setText(Messages.R_BUY_FAIL);
            }
        } else {
            sendMessage.setText(Messages.R_SHOP_EMPTY);
        }

        boardManager.applyDefaultKeyboard(sendMessage);

        return sendMessage;
    }

    private SendMessage shopAction(TelegramLongPollingBot bot, BoardManager boardManager, Update update) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        Long id = update.getMessage().getChatId();

        List<Product> productList = productService.findAll();

        sendMessage.setChatId(id);

        if (productList != null && !productList.isEmpty()) {
            sendMessage.setText(Messages.R_SHOP_1);
            boardManager.applyShopInlineKeyboard(sendMessage, productList);

        } else {
            sendMessage.setText(Messages.R_SHOP_EMPTY);
        }
        bot.execute(sendMessage);

        boardManager.applyShopKeyboard(sendMessage);
        DBUser user = userService.find(id);
        sendMessage.setText(Messages.R_SHOP_2 + user.getCash());

        return sendMessage;
    }

    private SendMessage eventAction(EventService eventService, BoardManager boardManager, Update update) {
        SendMessage sendMessage = new SendMessage();
        Long id = update.getMessage().getChatId();
        Event event = eventService.findLatest();

        if (event != null) {
            Integer count = eventService.countEventUsers(event);
            sendMessage.setText(event.getDescription()
                    .concat("\n")
                    .concat(Messages.R_EVENT_USERS)
                    .concat(count.toString())
            )
                    .setParseMode(ParseMode.HTML);

            boardManager.applyEventKeyboard(sendMessage);
        } else {
            sendMessage.setText(Messages.R_NO_EVENT)
                    .setParseMode(ParseMode.HTML);
            boardManager.applyDefaultKeyboard(sendMessage);
        }

        sendMessage.setChatId(id);

        return sendMessage;
    }

    private SendMessage refLinkAction(UserService userService, BoardManager boardManager, Update update) {
        Long id = update.getMessage().getFrom().getId().longValue();
        DBUser user = userService.find(id);

        SendMessage sendMessage = new SendMessage();

        if (user != null) {
            String finalText = Messages.R_PARTNER_1
                .concat(Messages.R_PARTNER_2 + Messages.R_REF_LINK + user.getTelegramId())
                .concat(Messages.R_PARTNER_3 + user.getRefCount());

            sendMessage.setText(finalText)
                .setChatId(id)
                .setParseMode(ParseMode.HTML);

            boardManager.applyDefaultKeyboard(sendMessage);
        } else {
            sendMessage = startAcrtion(userService, boardManager, update);
        }

        return sendMessage;
    }

    private SendMessage startAcrtion(UserService userService, BoardManager boardManager, Update update) {
        String text = update.getMessage().getText();
        Long id = update.getMessage().getFrom().getId().longValue();
        User fromUser = update.getMessage().getFrom();

        Long refId = RegexUtils.extractId(text);
        DBUser user = userService.find(id);
        SendMessage sendMessage = new SendMessage();

        if (user == null) {
            userService.add(extractDBUser(fromUser));
            if (refId != null) {
                DBUser refDBUser = userService.find(refId);
                if (refDBUser != null) {
                    int refCount = refDBUser.getRefCount();
                    refDBUser.setRefCount(++refCount);
                    double cash = refDBUser.getCash();
                    refDBUser.setCash(cash + DEFAULT_REF_CASH_INCREMENT);
                    userService.edit(refDBUser);
                }
            }
            sendMessage.setText(Messages.R_HELLO);
        } else {
            sendMessage.setText(Messages.R_WRONG_COMMAND);
        }
        boardManager.applyDefaultKeyboard(sendMessage);
        sendMessage.setChatId(id);

        return sendMessage;
    }

    private SendMessage helpAction(BoardManager boardManager, Update update) {
        Long id = update.getMessage().getFrom().getId().longValue();

        SendMessage sendMessage = new SendMessage();

        sendMessage.setChatId(id)
                .setText(Messages.R_HELP)
                .setParseMode(ParseMode.HTML);

        boardManager.applyDefaultKeyboard(sendMessage);

        return sendMessage;
    }

    private SendMessage newsAction(BoardManager boardManager, Update update) {
        SendMessage sendMessage = new SendMessage();

        sendMessage.setText(Messages.R_NEWS)
                .setChatId(update.getMessage().getChatId());
        boardManager.applyDefaultKeyboard(sendMessage);

        return sendMessage;
    }

    private SendMessage wrongAction(BoardManager boardManager, Update update, String errorMessage) {
        SendMessage sendMessage = new SendMessage();

        sendMessage.setText(errorMessage)
                .setChatId(update.getMessage().getChatId());
        boardManager.applyDefaultKeyboard(sendMessage);

        return sendMessage;
    }

    private DBUser extractDBUser(User user) {
        Long id = user.getId().longValue();
        String username = user.getUserName();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();

        DBUser dbUser = new DBUser(id, username, firstName, lastName, DEFAULT_REF_COUNT, Role.MEMBER.getRoleId(), null, DEFAULT_CASH_COUNT);
        return dbUser;
    }
}
