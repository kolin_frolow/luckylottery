package telegram.bot.luckylottery.bot.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.api.methods.send.SendDocument;
import org.telegram.telegrambots.api.objects.Update;
import telegram.bot.luckylottery.bot.AdminCommands;
import telegram.bot.luckylottery.entity.DBUser;
import telegram.bot.luckylottery.entity.Event;
import telegram.bot.luckylottery.entity.Role;
import telegram.bot.luckylottery.service.EventService;
import telegram.bot.luckylottery.service.UserService;
import telegram.bot.luckylottery.util.FileOutputUtils;

import java.io.File;
import java.util.List;

@Component("adminCommand")
public class AdminCommandsImpl implements AdminCommands {
    @Autowired
    protected EventService eventService;

    @Autowired
    protected UserService userService;

    @Override
    public SendDocument getEventUsersFile(Update update) {
        DBUser dbUser = userService.find(update.getMessage().getChatId());
        if (dbUser != null &&
                ( (dbUser.getRole() == Role.ADMIN.getRoleId()) || dbUser.getRole() == Role.MODER.getRoleId())) {
            Event event = eventService.findLatest();
            if (event != null) {
                List<DBUser> users = eventService.findEventUsers(event.getId());
                File file = FileOutputUtils.writeListToFile(users);
                SendDocument sendDocumentRequest = new SendDocument();

                sendDocumentRequest.setChatId(update.getMessage().getChatId());
                sendDocumentRequest.setNewDocument(file);
                sendDocumentRequest.setCaption("User list");

                return sendDocumentRequest;
            }
        }

        return null;
    }
}
