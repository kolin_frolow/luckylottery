package telegram.bot.luckylottery.bot;

import org.telegram.telegrambots.api.methods.send.SendDocument;
import org.telegram.telegrambots.api.objects.Update;
import telegram.bot.luckylottery.service.EventService;
import telegram.bot.luckylottery.service.UserService;

public interface AdminCommands {
    SendDocument getEventUsersFile(Update update);
}
