package telegram.bot.luckylottery.bot;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import telegram.bot.luckylottery.entity.Event;
import telegram.bot.luckylottery.entity.Product;

import java.util.List;

public interface BoardManager {
    SendMessage applyDefaultKeyboard(SendMessage sendMessage);

    SendMessage applyEventKeyboard(SendMessage sendMessage);

    SendMessage applyShopInlineKeyboard(SendMessage sendMessage, List<Product> productList);

    SendMessage applyShopKeyboard(SendMessage sendMessage);
}
