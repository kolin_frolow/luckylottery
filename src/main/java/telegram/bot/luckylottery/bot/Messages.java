package telegram.bot.luckylottery.bot;

import static telegram.bot.luckylottery.bot.Emoji.*;

public class Messages {
    private Messages() {}

    public static final String M_EVENT = SHIP + " Розыгрыш призов";
    public static final String M_HELP = BLACK_QUESTION_MARK_ORNAMENT + " О сервисе";
    public static final String M_BACK = BACK_WITH_LEFTWARDS_ARROW_ABOVE + " Назад";
    public static final String M_REF_LINK = HIGH_VOLTAGE_SIGN + " Партнерам";
    public static final String M_NEWS = AIRPLANE + " Новости";
    public static final String M_START = "/start";
    public static final String M_PARTICIPATE = Emoji.DOOR + "Участвовать в розыгрыше";
    public static final String M_SHOP = Emoji.ENVELOPE + "Магазин";
    public static final String M_SHOP_LIST = Emoji.INFORMATION_SOURCE + "Ваши покупки";

    public static final String A_GLOBAL = "/admin";
    public static final String A_USER_LIST = "/admin users";
    public static final String A_WAITING_LIST = "/admin shop";

    public static final String I_BUY = "/buy ";

    public static final String R_BUY_SUCCESS = "Товар добавляен в корзину. Ожидайте, с вами свяжутся.";
    public static final String R_BUY_FAIL = "Недостаточно средств.";

    public static final String R_SHOP_1 = "Список доступных товаров: ";
    public static final String R_SHOP_2 = "Ваш баланс: ";
    public static final String R_SHOP_EMPTY = "Сожалеем, в данный момент ничего не доступно. Приходите позже.";
    public static final String R_SHOP_LIST_EMPTY = "У вас нет заказынных товаров.";
    public static final String R_SHOP_LIST = "Список заказанных товаров: \n";


    public static final String R_REF_LINK = "t.me/LuckyLottery_bot?start=";
    public static final String R_WRONG_COMMAND = "Пожалуйста, используйте клавиатуру";
    public static final String R_HELLO = "Добро пожаловать, используйте меню для навигации";
    public static final String R_PARTNER_1 = "Приглашайте друзей и получайте бонусы.";
    public static final String R_PARTNER_2 = "\n<b>Ваша реферальная ссылка:</b> ";
    public static final String R_PARTNER_3 = "\n<b>Количество приглашенных друзей:</b> ";
    public static final String R_SUBSCRIBE = "Подписаться ";
    public static final String R_NEWS = "Самые актуальные новости о криптовалюе и не только: https://t.me/VS_INVEST ";
    public static final String R_NO_EVENT = "К сожалению, в данный момент никаких розыгрышей не провится. Приходите позже.";
    public static final String R_NO_ACCESS = "К сожалению, ваших прав не достаточно. Свяжитесь с нами по почте - support@email.com.";
    public static final String R_HELP = "Данный бот предназначен для розыгрыша различных призов.\n" +
            "По вопроса рекламы пишите login@email.com";
    public static final String R_EVENT_USERS = "Количество участников: ";
    public static final String R_PARTICIPATION_SUCCESSFULL = "Поздравляем, вы зарегестрированы в розыгрыше!";
    public static final String R_PARTICIPATION_FAIL = "Вы не выполнили все условия.";
    public static final String R_PARTICIPATION_ALREADY = "Вы уже участвуете в розыгрыше! Ожидайте.";
}
