package telegram.bot.luckylottery.bot;

public class ParseMode {
    private ParseMode() {}

    public static final String HTML = "html";
    public static final String MARKDOWN = "markdown";
}
