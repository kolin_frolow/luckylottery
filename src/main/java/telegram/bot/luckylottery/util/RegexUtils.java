package telegram.bot.luckylottery.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtils {
    private static final String REGEX_ARGUMENT = "(?<=^\\/buy ).*$";
    private static final String REGEX_ID = "\\d{9}";

    public static Long extractId(String command) {
        Pattern pattern = Pattern.compile(REGEX_ID);
        Matcher matcher = pattern.matcher(command);
        if (matcher.find()) {
            return Long.parseLong(matcher.group(0));
        } else {
            return null;
        }
    }

    //TODO: dynamic String concat
    public static Long extractArgument(String command) {
        Pattern pattern = Pattern.compile(REGEX_ARGUMENT);
        Matcher matcher = pattern.matcher(command);
        if (matcher.find()) {
            return Long.parseLong(matcher.group(0));
        } else {
            return 0L;
        }
    }
}
