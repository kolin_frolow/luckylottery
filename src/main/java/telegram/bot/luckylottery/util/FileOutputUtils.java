package telegram.bot.luckylottery.util;

import telegram.bot.luckylottery.entity.DBUser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class FileOutputUtils {
    private static final String PATH = "./tmp.txt";

    public static File writeListToFile(List<DBUser> list) {
        try {
            FileWriter fileWriter = new FileWriter(PATH);
            PrintWriter printWriter = new PrintWriter(fileWriter);

            int i = 0;
            for (DBUser user:
                 list) {
                printWriter.println(i + " " + user);
                ++i;
            }

            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new File(PATH);
    }
}
