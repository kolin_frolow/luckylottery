package telegram.bot.luckylottery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;
import telegram.bot.luckylottery.starter.EnableTelegramBots;

@SpringBootApplication
@EnableTelegramBots
public class LuckyBotApplication {
    public static void main(String[] args) {
        ApiContextInitializer.init();

        SpringApplication.run(LuckyBotApplication.class, args);
    }
}
